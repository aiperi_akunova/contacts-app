import axios from "axios";

export const CONTACTS_REQUEST = 'CONTACTS_REQUEST';
export const CONTACTS_SUCCESS = 'CONTACTS_SUCCESS';
export const CONTACTS_FAILURE = 'CONTACTS_FAILURE';

export const contactsRequest = ()=>({type:CONTACTS_REQUEST})
export const contactsSuccess = data=>({type:CONTACTS_SUCCESS, payload: data})
export const contactsFailure = error=>({type:CONTACTS_FAILURE, payload:error});

export const ADD_CONTACT_REQUEST = 'ORDER_REQUEST';
export const ADD_CONTACT_SUCCESS = 'ORDER_SUCCESS';
export const ADD_CONTACT_FAILURE = 'ORDER_FAILURE';

export const addContactRequest = ()=>({type:ADD_CONTACT_REQUEST})
export const addContactSuccess = ()=>({type:ADD_CONTACT_SUCCESS})
export const addContactFailure = error=>({type:ADD_CONTACT_FAILURE, payload:error});



export const getContacts = () =>{
    return async dispatch =>{
        try{
            dispatch(contactsRequest());
            const response = await axios.get('https://exam-40090-default-rtdb.firebaseio.com/contacts.json');
            dispatch(contactsSuccess(response.data));
        } catch (error){
            dispatch(contactsFailure(error))
        }
    }
}

export const postContacts = contactData =>{
    return async dispatch =>{
        try{
            dispatch(addContactRequest());
            await axios.post('https://exam-40090-default-rtdb.firebaseio.com/contacts.json', contactData);
            dispatch(addContactSuccess())
        } catch (error){
            dispatch(addContactFailure(error))
        }
    }
}