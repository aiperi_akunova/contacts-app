import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Contacts from "./container/Contacts/Contacts";
import AddContact from "./container/AddContact/AddContact";
import Layout from "./Layout/Layout";

const App = () => (
   <BrowserRouter>
       <Switch>
           <Layout>
               <Route path='/' exact component={Contacts}/>
               <Route path='/newContact' component={AddContact}/>
           </Layout>
       </Switch>
   </BrowserRouter>
);

export default App;
