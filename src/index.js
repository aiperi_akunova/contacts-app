import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import {Provider} from 'react-redux';
import contactsReducer from './store/reducer/contactsReducer.js'

const store = createStore(contactsReducer, applyMiddleware(thunkMiddleware));

const app = (
    <Provider store={store}>
            <App/>
    </Provider>
)

ReactDOM.render(app,document.getElementById('root')
);


