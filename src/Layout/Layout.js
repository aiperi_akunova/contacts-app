import React from 'react';
import './Layout.css';
import {NavLink} from "react-router-dom";

const Layout = ({children}) => {
    return (
        <div className='App'>
            <header className='header'>
               <NavLink to='/' className='logo'>Contacts</NavLink>
                <NavLink to='/newContact' className='add-btn'>Add new contact</NavLink>
            </header>
            <main className='Layout-box'>
                {children}
            </main>
            
        </div>
    );
};

export default Layout;