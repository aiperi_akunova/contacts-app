import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getContacts} from "../../store/actions/contactsActions";
import ContactShow from "../../components/ContactShow/ContactShow";
import Modal from "../../components/UI/Modal/Modal";
import './Contacts.css';

const Contacts = () => {
    const dispatch =useDispatch();
    const contacts = useSelector(state => state.contacts);

    const [oneContact, setOneContact] = useState(null);

    const [modalShow, setModalShow] = useState(false);
    useEffect(()=>{
        const fetchData = async ()=>{
            await  dispatch(getContacts());
        }
        fetchData();
    },[dispatch]);

    const onContactClick =(value)=>{
        setModalShow(true);
        setOneContact(value);
    }

    const modalCancel =()=>{
        setModalShow(false);
    }

    return (
        <div>

            {contacts && Object.keys(contacts).map(c=>(
                <ContactShow
                    key ={c}
                    name = {contacts[c].name}
                image={contacts[c].photo}
                    onClick = {()=>{onContactClick(contacts[c])}}
                />
                ))}
            <Modal
                show={modalShow}
                close={modalCancel}
            >
                {oneContact &&  <div className='avatar'>
                    <img src={oneContact.photo} alt="avatar"/>
                    <h3>{oneContact.name}</h3>
                    <p>{oneContact.email}</p>
                    <p>{oneContact.phone}</p>
                    <button>Edit</button>
                    <button>Delete</button>
                </div>}
            </Modal>

        </div>
    );
};

export default Contacts;