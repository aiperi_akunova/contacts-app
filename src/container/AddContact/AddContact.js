import React, {useState} from 'react';
import './AddContact.css';
import {useDispatch} from "react-redux";
import {postContacts} from "../../store/actions/contactsActions";

const AddContact = ({history}) => {
    const dispatch = useDispatch();

    const [contact, setContact] = useState({
        name: '',
        phone: '',
        email: '',
        photo: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setContact(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const onCancel =()=>{
        history.replace('/');
    }

    const onSave =e=>{
        e.preventDefault();
        dispatch(postContacts(contact));
        history.replace('/');
    }

    return (
        <div className='New-Contact'>
           <h2>Add new contact</h2>
            <form>
                <label htmlFor="name">Name:</label>
                <input
                    type='text'
                    name='name'
                    value={contact.name}
                    onChange={onInputChange}
                    autoComplete='off'
                />
                <label htmlFor="phone">Phone:</label>
                <input
                    type='tel'
                    pattern='[0-9]{4}-[0-9]{3}-[0-9]{3}'
                    name='phone'
                    value={contact.phone}
                    onChange={onInputChange}
                    autoComplete='off'
                />
                <label htmlFor="email">Email:</label>
                <input
                    type='email'
                    name='email'
                    value={contact.email}
                    onChange={onInputChange}
                    autoComplete='off'
                />
                <label htmlFor="photo">Photo:</label>
                <input
                    type='email'
                    name='photo'
                    value={contact.photo}
                    onChange={onInputChange}
                    autoComplete='off'
                />
                <div className='preview'>
                    <p>Preview:</p>
                    <img src={contact.photo ? contact.photo : 'https://hope.be/wp-content/uploads/2015/05/no-user-image.gif'} alt=""/>
                </div>

                <div className='buttons'>
                    <button onClick={e=>onSave(e)}>Save</button>
                    <button onClick={onCancel}>Back to contacts</button>
                </div>
            </form>
        </div>
    );
};

export default AddContact;