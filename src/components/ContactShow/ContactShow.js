import React from 'react';
import './ContactShow.css';

const ContactShow = props => {
    return (
        <div className='Contact-box' onClick={props.onClick}>
            <img src={props.image} alt="Contact"/>
            <h3>{props.name}</h3>
        </div>
    );
};

export default ContactShow;